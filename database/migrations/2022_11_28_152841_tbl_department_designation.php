<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblDepartmentDesignation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('tbl_department-designation', function (Blueprint $table)
        {
            $table->foreignId('department_id')
                  ->constrained('tbl_department')->cascadeOnDelete()
            ;
            $table->foreignId('designation_id')
                  ->constrained('tbl_designation')->cascadeOnDelete()
            ;
            $table->datetime('created_at')->useCurrent();
            $table->datetime('updated_at')->useCurrent()->useCurrentOnUpdate();
        });
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
