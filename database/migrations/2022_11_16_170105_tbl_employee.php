<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblEmployee extends Migration
{
    public function up()
    {
        Schema::create('tbl_employee', function (Blueprint $table)
        {
            $table->id()->unsigned();
            $table->string('employee_name',500);
            $table->string('email',500);
            $table->bigInteger('department_id')->default(0);
            $table->bigInteger('designation_id')->default(0);
            $table->boolean('is_confirm')->default(0);
            $table->boolean('is_deleted')->default(0);
            $table->datetime('created_at')->useCurrent();
            $table->datetime('updated_at')->useCurrent()->useCurrentOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('tbl_employee');
    }

    }
