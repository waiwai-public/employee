<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
        .button {
            background-color: #4CAF50;
            border: none;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
        }
    </style>

    <title>Employee</title>

    <script type="text/javascript">

        var alertTrigger = null;
        @if(isset($status))
                @if($status==204)
            alertTrigger="Success";
        @endif
                @if($status==409)
            alertTrigger="Duplicate Id";
        @endif
                @if($status==403)
            alertTrigger="Invalid Value";
        @endif
        @endif

        function runAlert(alertTrigger)
        {
            if(alertTrigger!=null)
            {
                alert(alertTrigger);
            }
            alertTrigger=null;
        }


        runAlert(alertTrigger);

    </script>

</head>

<body>
<a href="{{Route("employee.list")}}">Go To List</a>
<br/>
<a href="{{Route("department-designation.list")}}">Link Department Designation</a>
<br/>
<a href="{{Route("department.add")}}">Add Department</a>
<br/>
<a href="{{Route("designation.add")}}">Add Designation</a>
<br/>
<div>
    <form action="{{Route("department.insert")}}" method="post">
        {{csrf_field()}}
        <table>
            <tr>
                <td><label for="department_name">Department Name:</label></td>
                <td><input type="text" id="department_name" name="department_name" value="" required></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <button id="submit_button" type="submit">Submit</button>
                </td>
            </tr>
        </table>
    </form>
</div>
</body>

</html>
