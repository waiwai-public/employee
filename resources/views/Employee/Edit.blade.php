<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
        .button {
            background-color: #4CAF50;
            border: none;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
        }
    </style>

<title>Employee</title>

    <script type="text/javascript">

        var alertTrigger = null;
        @if(isset($status))
            @if($status==204)
                alertTrigger="Success";
            @endif
            @if($status==404)
                alertTrigger="Not Found";
            @endif
            @if($status==409)
                alertTrigger="Duplicate Id";
            @endif
            @if($status==403)
                alertTrigger="Invalid Value";
            @endif
        @endif

        function runAlert(alertTrigger)
        {
            if(alertTrigger!=null)
            {
                alert(alertTrigger);
            }
            alertTrigger=null;
        }

        runAlert(alertTrigger);

    </script>

</head>

<body>
<div>
    <a href="{{Route("employee.list")}}">Go To List</a>
    <form id="form" action="{{Route("employee.update",['id'=>$employee->id])}}" method="post">
        {{csrf_field()}}
        @method('PUT')
        <table>
        <tr>
            <td><label for="id">Employee ID:</label></td>
            <td><input type="text" id="id" name="id" value="{{$employee->id}}" required></td>
        </tr>

                     <tr>
            <td><label for="employee_name">Employee Name:</label></td>
            <td><input type="text" id="employee_name" name="employee_name" value="{{$employee->employee_name}}" required></td>
        </tr>

                <tr>
            <td><label for="email">Email:</label></td>
            <td><input type="text" id="email" name="email" value="{{$employee->email}}" required></td>
        </tr>


        <tr>
            <td><label for="email">Department :</label></td>
            <td><select name ="department_id">
                    @foreach($departments as $departmentKey => $departmentName)
                        <option value="{{$departmentKey}}" @if($employee->department_id===$departmentKey)selected @endif >{{$departmentName}}</option>
                    @endforeach
                </select></td>
        </tr>


        <tr>
            <td> <label for="email">Designation :</label><br></td>
            <td> <select name ="designation_id">
                    @foreach($designations as $designationId=>$designationKey)
                        <option value="{{$designationId}}" @if($employee->designation_id===$designationKey)selected @endif>{{$designationKey}}</option>
                    @endforeach
                </select></td>
        </tr>
                <tr>
            <td> <label for="is_confirm">Is Confirm Staff</label><br></td>
            <td> <input name="is_confirm" type="Checkbox" @if($employee->is_confirm===true) Checked @endif></td>
        </tr>
            <tr>
                <td></td>
                <td>
                <button id="submit_button" type="submit">Submit</button>
                </td>
            </tr>
        </table>
    </form>

</div>
</body>
</html>
