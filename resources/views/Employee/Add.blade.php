<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
        .button {
            background-color: #4CAF50;
            border: none;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
        }
    </style>

    <title>Employee</title>

    <script type="text/javascript">

        var alertTrigger = null;
        @if(isset($status))
                @if($status==204)
            alertTrigger="Success";
                @endif
                @if($status==409)
            alertTrigger="Duplicate Id";
                @endif
                @if($status==403)
            alertTrigger="Invalid Value";
        @endif
        @endif

        function runAlert(alertTrigger)
        {
            if(alertTrigger!=null)
            {
                alert(alertTrigger);
            }
            alertTrigger=null;
        }


        function getDesignation()
        {
            var departmentDesignation = {!! json_encode($departmentDesignations) !!};
            document.getElementById("designation_id").options.length=0;
            console.table(departmentDesignation[document.getElementById("department_id").value])
            departmentDesignation[document.getElementById("department_id").value].forEach(setDesignation)
        }

        function setDesignation(value)
        {
            var designations = {!! json_encode($designations) !!};
            var designationSelect =   document.getElementById("designation_id");
            designationSelect.options[designationSelect.options.length] = new Option(designations[value], value);
        }
        window.onload = function(){getDesignation()};
        runAlert(alertTrigger);

    </script>

</head>

<body>

<a href="{{Route("employee.list")}}">Go To List</a>
<div>
    <form action="{{Route("employee.insert")}}" method="post">
        {{csrf_field()}}
        <table>
            <tr>
                <td><label for="id">Employee ID:</label></td>
                <td><input type="number" id="id" name="id" value="" required></td>
            </tr><tr>
                <td><label for="employee_name">Employee Name:</label></td>
                <td><input type="text" id="employee_name" name="employee_name" value="" required></td>
            </tr>

            <tr>
                <td><label for="email">Email:</label></td>
                <td><input type="text" id="email" name="email" value="" required></td>
            </tr>


            <tr>
                <td><label for="email">Department :</label></td>
                <td><select id="department_id" name ="department_id" onchange="getDesignation()" required>
                        @foreach($departments as $departmentKey => $departmentName)
                            <option value="{{$departmentKey}}" >{{$departmentName}}</option>
                        @endforeach
                    </select></td>
            </tr>


            <tr>
                <td> <label for="email">Designation :</label><br></td>
                <td>
                    <select id="designation_id" name ="designation_id" required>
                        @foreach($designations as $designationId=>$designationKey)
                            <option value="{{$designationId}}">{{$designationKey}}</option>
                        @endforeach
                    </select></td>
            </tr>
            <tr>
                <td> <label for="is_confirm">Is Confirm Staff</label><br></td>
                <td> <input name="is_confirm" type="Checkbox" ></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <button id="submit_button" type="submit">Submit</button>
                </td>
            </tr>
        </table>
    </form>
</div>
</body>

</html>
