<html>
<head>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
      .button {
          background-color: #4CAF50;
          border: none;
          color: white;
          padding: 15px 32px;
          text-align: center;
          text-decoration: none;
          display: inline-block;
          font-size: 16px;
          margin: 4px 2px;
          cursor: pointer;
      }
    </style>
    <title>Listing</title>

    <script type="text/javascript">

        var alertTrigger = null;
        @if(isset($status))
                @if($status==204)
                    alertTrigger="Success";
                @endif
                @if($status==200)
                    alertTrigger="Success";
                @endif
                @if($status==404)
                    alertTrigger="Not Found";
                @endif
        @endif

        function runAlert(alertTrigger)
        {
            if(alertTrigger!=null)
            {
                alert(alertTrigger);
            }
            alertTrigger=null;
        }

        runAlert(alertTrigger);

    </script>
</head>
<body>
<a href="{{Route("employee.list")}}">Go To List</a>
<br/>
<a href="{{Route("department-designation.list")}}">Link Department Designation</a>
<br/>
<a href="{{Route("department.add")}}">Add Department</a>
<br/>
<a href="{{Route("designation.add")}}">Add Designation</a>
<br/>
{{csrf_field()}}
<table>
    <tr>
        <td>ID</td>
        <td>Emp Name</td>
        <td>EMAIL ADDRESS</td>
        <td>DEPARTMENT</td>
        <td>DESIGNATION</td>
        <td>CONFIRM</td>
        <td>ACTION</td>
    </tr>
    @foreach($employees as $employee)
        <tr>
        <td>{{$employee->id}}</td>
        <td>{{$employee->employee_name}}</td>
        <td>{{$employee->email}}</td>
        <td>
{{$departments[$employee->department_id]}}
        </td>
        <td>
            {{$designations[$employee->designation_id]}}</td>
        <td><input type="Checkbox"  @if($employee->is_confirm==true) checked @endif ></td>
        <td> <form action="{{Route('employee.delete',['id'=>$employee->id])}}" method="post"><a href="{{Route('employee.edit',['id'=>$employee->id])}}" class="button">Edit</a>

            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <button class="button" onclick="return confirm('Are you sure you want to delete this item?');">Delete</button>
        </form>
        </td>
        </tr>
    @endforeach

</table>
<a href="{{Route('employee.add')}}" class="button" style="float: right">Add</a>
</body>
</html>