<?php

    declare(strict_types = 1);

    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | contains the "web" middleware group. Now create something great!
    |
    */


    use App\Http\Controllers\Department\GetDepartmentAddController;
    use App\Http\Controllers\Department\PostDepartmentController;
    use App\Http\Controllers\DepartmentDesignation\GetDepartmentDesignationController;
    use App\Http\Controllers\DepartmentDesignation\PostDepartmentDesignationController;
    use App\Http\Controllers\Designation\GetDesignationAddController;
    use App\Http\Controllers\Designation\PostDesignationController;
    use Illuminate\Support\Facades\Route;

    Route ::GET('/', [\App\Http\Controllers\Employee\GetEmployeeListController::class, 'execute']);
    Route ::GET('employee', [\App\Http\Controllers\Employee\GetEmployeeListController::class, 'execute'])
          -> name('employee.list');
    Route ::GET('employee/list', [\App\Http\Controllers\Employee\GetEmployeeListController::class, 'execute'])
          -> name('employee.list');

    Route ::GET('employee/add', [\App\Http\Controllers\Employee\GetEmployeeAddController::class, 'execute'])
          -> name('employee.add');

    Route ::GET('employee/edit/{id}', [\App\Http\Controllers\Employee\GetEmployeeEditController::class, 'execute'])
          -> name('employee.edit');

    Route ::POST('employee/insert', [\App\Http\Controllers\Employee\PostEmployeeController::class, 'execute'])
          -> name('employee.insert');

    Route ::Put('employee/update/{id}', [\App\Http\Controllers\Employee\PutEmployeeController::class, 'execute'])
          -> name('employee.update');

    Route ::DELETE('employee/delete/{id}', [\App\Http\Controllers\Employee\DeleteEmployeeController::class, 'execute'])
          -> name('employee.delete');


    Route ::GET('department-designation/list',
                [GetDepartmentDesignationController::class, 'execute'])
          -> name('department-designation.list');
    Route ::Post('department-designation/link',
                [PostDepartmentDesignationController::class, 'execute'])
          -> name('department-designation.link');

    Route ::Post('department/insert',
                [PostDepartmentController::class, 'execute'])
          -> name('department.insert');

    Route ::GET('department/add',
                [GetDepartmentAddController::class, 'execute'])
          -> name('department.add');

    Route ::Post('designation/insert',
                [PostDesignationController::class, 'execute'])
          -> name('designation.insert');

    Route ::GET('designation/add',
                [GetDesignationAddController::class, 'execute'])
          -> name('designation.add');
