<?php

    namespace App\Models;

    use Illuminate\Database\Eloquent\Model;

    class Designation extends Model
    {

        protected $table = 'tbl_designation';

        protected $fillable = [
            'designation_name',
            'is_deleted',
            'created_at',
            'updated_at',
        ];
        protected $dates    = ['created_at', 'updated_at'];


    }