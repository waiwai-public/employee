<?php

    namespace App\Models;

    use Illuminate\Database\Eloquent\Model;

    class Employee extends Model
    {

        protected $table = 'tbl_employee';

        protected $fillable = [
            'id',
            'employee_name',
            'email',
            'department_id',
            'designation_id',
            'is_confirm',
            'is_deleted',
            'created_at',
            'updated_at',
        ];

        protected $casts = [
            'employee_name'=>"string",
            'is_confirm'=>"boolean",

        ];

        protected $dates    = ['created_at', 'updated_at'];


    }