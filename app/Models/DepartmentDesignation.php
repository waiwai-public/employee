<?php

    namespace App\Models;

    use Illuminate\Database\Eloquent\Model;

    class DepartmentDesignation extends Model
    {
        protected $table = 'tbl_department-designation';

        protected $fillable = [
            'department_id',
            'designation_id',
            'created_at',
            'updated_at',
        ];
        protected $dates    = ['created_at', 'updated_at'];

    }