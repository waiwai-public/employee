<?php

    namespace App\Models;

    use Illuminate\Database\Eloquent\Model;

    class Department extends Model
    {
        protected $table    = "tbl_department";
        protected $fillable = [
            'department_name',
            'is_deleted',
            'created_at',
            'updated_at',
        ];
        protected $dates    = ['created_at', 'updated_at'];


    }