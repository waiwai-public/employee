<?php

    namespace App\Classes\Exception;

    class ResourceConflictException extends \ErrorException
    {
        public function __construct()
        {
            parent::__construct('Duplicate Record');
        }
    }