<?php

    namespace App\Classes\ControllerLogic\Department;

    use App\Classes\Exception\ResourceConflictException;
    use App\Classes\Validator\DepartmentValidator;
    use App\Models\Department;

    class PostDepartmentLogic
    {

        public function __construct(private Department $department,

        private DepartmentValidator $departmentValidator) { }

        public
        function execute(\Illuminate\Http\Request $request
        ): void {

            $this->departmentValidator->execute($request);

            $department = $this->department->newQuery()->where('department_name','=',$request->get('department_name')
                )->get()->first();

            if($department!==null)
            {
                throw new ResourceConflictException();
            }


            $instance = $this->department->newInstance();
            
            $instance->fill(['department_name'=>$request->get('department_name'),'is_deleted'=>0]);
            $instance->save();
        }


    }