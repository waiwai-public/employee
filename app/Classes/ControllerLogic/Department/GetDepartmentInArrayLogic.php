<?php
    namespace App\Classes\ControllerLogic\Department;

    use App\Models\Department;
    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Support\Collection;

    class GetDepartmentInArrayLogic
    {
        public function __construct(private Department $department) { }

        public
        function execute():Collection
        {
            return $this->department->newQuery()->where('is_deleted','=',false)->get()->mapWithKeys
            (function($department){return [$department->id => $department->department_name];});

        }


    }