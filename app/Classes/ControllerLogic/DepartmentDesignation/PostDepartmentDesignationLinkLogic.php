<?php

    namespace App\Classes\ControllerLogic\DepartmentDesignation;

    use App\Classes\Exception\ResourceConflictException;
    use App\Classes\Validator\DepartmentDesignationValidator;
    use App\Models\Department;
    use App\Models\DepartmentDesignation;
    use App\Models\Designation;
    use Illuminate\Database\Eloquent\ModelNotFoundException;
    use Illuminate\Http\Request;

    class PostDepartmentDesignationLinkLogic
    {
        public function __construct(private DepartmentDesignation $departmentDesignation,
        private DepartmentDesignationValidator $departmentDesignationValidator,
        private Department $department, private Designation $designation) { }
        public function execute(Request $request):void
        {
            $this->departmentDesignationValidator->execute($request);

            $department = $this->department->newQuery()->where('is_deleted','=',0)->find($request->get('department_id'));
            if($department===null)
            {
                throw new ModelNotFoundException();
            }
            $designation = $this->designation->newQuery()->where('is_deleted','=',0)->find($request->get('designation_id'));
            if($designation===null)
            {
                throw new ModelNotFoundException();
            }

            $instance = $this->departmentDesignation->newInstance();
            $instance->fill([
                                'department_id'=>$request->get('department_id'),
                                'designation_id'=>$request->get('designation_id'),
                            ]);
            $instance->save();

            return;
        }


    }