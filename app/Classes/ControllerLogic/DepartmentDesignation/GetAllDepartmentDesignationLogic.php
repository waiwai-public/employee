<?php

    namespace App\Classes\ControllerLogic\DepartmentDesignation;

    use App\Models\DepartmentDesignation;
    use Illuminate\Http\Request;

    class GetAllDepartmentDesignationLogic
    {
public function __construct(private DepartmentDesignation $departmentDesignation) { }
        public function execute():array
        {
            $departmentDesignations = $this->departmentDesignation->newQuery()->get();
            return  $this->mapDepartmentAsKeyDesignationAsArrayValue($departmentDesignations);
        }

        private
        function mapDepartmentAsKeyDesignationAsArrayValue(\Illuminate\Database\Eloquent\Collection|array
                                                           $departmentDesignations
        ) {

            $mappedDepartment=array();
            /** @var  $departmentDesignation */
            foreach ($departmentDesignations as $departmentDesignation)
            {
                $mappedDepartment[$departmentDesignation->department_id][]=$departmentDesignation->designation_id;
            }
            return $mappedDepartment;
        }


    }