<?php

    namespace App\Classes\ControllerLogic\Employee;

    use App\Classes\Exception\ResourceConflictException;
    use App\Classes\Validator\EmployeeValidator;
    use App\Models\Department;
    use App\Models\Employee;
    use Illuminate\Database\Eloquent\ModelNotFoundException;
    use Illuminate\Http\Request;
    use Illuminate\Validation\ValidationException;

    class PostEmployeeLogic
    {
        public function __construct(private EmployeeValidator $employeeValidator,private Employee $employee ) { }

        public function execute(Request $request)
        {

                 $this->employeeValidator->execute($request);


                $employee = $this->employee->newQuery()->find($request->get('id'));
                if($employee!==null) {
                    throw new ResourceConflictException();
                }
                    $instance = $this->employee->newInstance();
                    $instance->fill([
                                        'id'=>$request->get("id"),
                                        'employee_name'=> $request->get('employee_name'),
                                        'email'=>$request->get('email'),
                                        'department_id'=>$request->get('department_id'),
                                        'designation_id'=>$request->get('designation_id'),
                                        'is_confirm'=>$request->get('is_confirm')?true:false,
                                    ]);
                    $instance->save();
                    return $instance;

        }


    }