<?php
namespace App\Classes\ControllerLogic\Employee;

use App\Models\Employee;
use Illuminate\Support\Collection;

class GetEmployeeLogic
    {

        public function __construct(private Employee $employee) { }

        public
        function execute():Collection
        {
            return $this->employee->newQuery()->where('is_deleted','=',false)->get();
        }

}