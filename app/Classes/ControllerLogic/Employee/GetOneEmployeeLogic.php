<?php
namespace App\Classes\ControllerLogic\Employee;

use App\Models\Employee;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class GetOneEmployeeLogic
    {

        public function __construct(private Employee $employee) { }

        public
        function execute(int $id):Model
        {
            $employee =  $this->employee->newQuery()->where('is_deleted','=',false)->find($id);
            if($employee===null)
            {
                throw new ModelNotFoundException();
            }
            return $employee;
        }

}