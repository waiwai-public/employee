<?php

    namespace App\Classes\ControllerLogic\Employee;

    use App\Models\Employee;
    use Illuminate\Support\ItemNotFoundException;

    class DeleteEmployeeLogic
    {
        public function __construct(private Employee $employee) { }
        public function execute(int $id):Bool
        {
            $instance = $this -> employee -> newQuery() -> find($id);
            if ($instance === null) {
                throw new ItemNotFoundException();
            }
            $instance -> fill([
                                  'is_deleted' => true,
                              ]);
            $instance -> save();
            return true;
        }


    }