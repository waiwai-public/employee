<?php

    namespace App\Classes\ControllerLogic\Designation;

    use App\Classes\Exception\ResourceConflictException;
    use App\Classes\Validator\DesignationValidator;
    use App\Models\Designation;

    class PostDesignationLogic
    {

        public function __construct(private Designation $designation,

        private DesignationValidator $designationValidator) { }

        public
        function execute(\Illuminate\Http\Request $request
        ): void {

            $this->designationValidator->execute($request);

            $designation = $this->designation->newQuery()->where('designation_name','=',$request->get('designation_name'))                        ->get()->first();

            if($designation!==null)

            {
                throw new ResourceConflictException();
            }


            $instance = $this->designation->newInstance();
            
            $instance->fill(['designation_name'=>$request->get('designation_name'),'is_deleted'=>0]);
            $instance->save();
        }


    }