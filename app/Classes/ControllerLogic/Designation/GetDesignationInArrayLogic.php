<?php

    namespace App\Classes\ControllerLogic\Designation;

    use App\Models\Designation;
    use Illuminate\Support\Collection;

    class GetDesignationInArrayLogic
    {
        public function __construct(private Designation $designation) { }

        public
        function execute():Collection
        {
            return $this->designation->newQuery()->where('is_deleted','=',false)->get()->mapWithKeys
            (function($designation){return [$designation->id => $designation->designation_name];});
        }

    }