<?php

    namespace App\Classes\Validator;

    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Validator;
    use Illuminate\Validation\ValidationException;

    class DepartmentValidator
    {
        public function execute(Request $request)
        {

            $validator = Validator::make($request->all(), [
                'department_name' => 'string|required',
            ]);
            if ($validator->fails()) {
                throw new ValidationException($validator);
            }
            return true;
        }
    }