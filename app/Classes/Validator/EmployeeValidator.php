<?php
namespace App\Classes\Validator;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Validator;
    use Illuminate\Validation\ValidationException;

    class EmployeeValidator
    {

        public function execute(Request $request)
        {

            $validator = Validator::make($request->all(), [
                'id'=>'integer',
                'employee_name' => 'required|max:255',
                'department_id' => 'integer|required',
                'designation_id' => 'integer|required',
                'email' => 'email|required',
            ]);
            if ($validator->fails()) {
                throw new ValidationException($validator);
            }
            return true;
        }
    }