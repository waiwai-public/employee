<?php

    namespace App\Classes\Validator;

    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Validator;
    use Illuminate\Validation\ValidationException;

    class DepartmentDesignationValidator
    {
        public function execute(Request $request)
        {

            $validator = Validator::make($request->all(), [
                'department_id' => 'integer|required',
                'designation_id' => 'integer|required',
            ]);
            if ($validator->fails()) {
                throw new ValidationException($validator);
            }
            return true;
        }
    }