<?php

    namespace App\Http\Controllers\Designation;
    use App\Classes\ControllerLogic\Designation\PostDesignationLogic;
    use App\Classes\Exception\ResourceConflictException;
    use App\Http\Controllers\Controller;
    use Illuminate\Http\Request;
    use Illuminate\Validation\ValidationException;

    class PostDesignationController extends Controller
    {
public function __construct(private PostDesignationLogic $postDesignationLogic) { }
        public function execute(Request $request)
        {

            try{
                $this->postDesignationLogic->execute($request);
                $status = 204;
            }catch (ResourceConflictException)
            {
                $status = 409;

            }catch (ValidationException)
            {
                $status = 403;
            }

            return redirect(route('designation.add',['status'=>$status]));

        }


    }