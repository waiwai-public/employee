<?php

    namespace App\Http\Controllers\Designation;

    use Illuminate\View\View;

    class GetDesignationAddController
    {
public function __construct() { }
        public function execute():View
        {

            return view('Designation/add');
        }


    }