<?php

    namespace App\Http\Controllers\DepartmentDesignation;
    use App\Classes\ControllerLogic\Department\GetDepartmentInArrayLogic;
    use App\Classes\ControllerLogic\DepartmentDesignation\PostDepartmentDesignationLinkLogic;
    use App\Classes\ControllerLogic\Designation\GetDesignationInArrayLogic;
    use App\Http\Controllers\Controller;
    use Illuminate\Database\Eloquent\ModelNotFoundException;
    use Illuminate\Http\RedirectResponse;
    use Illuminate\Http\Request;
    use Illuminate\Http\Response;
    use Illuminate\Validation\ValidationException;
    use Illuminate\View\View;

    class PostDepartmentDesignationController extends Controller
    {

        public function __construct(
            private PostDepartmentDesignationLinkLogic $postDepartmentDesignationLinkLogic
        ) { }
        public function execute(Request $request):RedirectResponse
        {

            try{
                $this->postDepartmentDesignationLinkLogic->execute($request);
                $status = 204;

            }catch (ModelNotFoundException)
            {
                $status = 404;
            }
            catch (ValidationException)
            {
                $status = 403;
            }
            return redirect(route('department-designation.list',['status'=>$status]));
        }


    }