<?php

    namespace App\Http\Controllers\DepartmentDesignation;
    use App\Classes\ControllerLogic\Department\GetDepartmentInArrayLogic;
    use App\Classes\ControllerLogic\Designation\GetDesignationInArrayLogic;
    use App\Http\Controllers\Controller;
    use Illuminate\View\View;

    class GetDepartmentDesignationController extends Controller
    {

        public function __construct(private GetDepartmentInArrayLogic $getDepartmentInArrayLogic, private
        GetDesignationInArrayLogic
        $getDesignationInArrayLogic,) { }
        public function execute():View
        {
            $department = $this->getDepartmentInArrayLogic->execute();
            $designation = $this->getDesignationInArrayLogic->execute();


            return view('DepartmentDesignation/list')->with('departments',$department)->with('designations',
                                                                                        $designation);
        }


    }