<?php

    namespace App\Http\Controllers\Employee;

    use App\Classes\ControllerLogic\Department\GetDepartmentInArrayLogic;
    use App\Classes\ControllerLogic\DepartmentDesignation\GetAllDepartmentDesignationLogic;
    use App\Classes\ControllerLogic\Designation\GetDesignationInArrayLogic;
    use Illuminate\View\View;

    class GetEmployeeAddController
    {
        public function __construct(
            private GetDepartmentInArrayLogic  $getDepartmentLogic,
            private GetDesignationInArrayLogic $getDesignationLogic,
        private GetAllDepartmentDesignationLogic $getAllDepartmentDesignationLogic) { }
        public function execute():View
        {
            $departmentCollection = $this ->getDepartmentLogic->execute();
            $designationCollection = $this ->getDesignationLogic->execute();
            $departmentDesignationArray = $this->getAllDepartmentDesignationLogic->execute();

            return view('Employee\Add')
                ->with('departments',$departmentCollection)
                ->with('designations',$designationCollection)
                ->with('departmentDesignations',$departmentDesignationArray);
        }
    }