<?php

    namespace App\Http\Controllers\Employee;

    use App\Classes\ControllerLogic\Department\GetDepartmentInArrayLogic;
    use App\Classes\ControllerLogic\Designation\GetDesignationInArrayLogic;
    use App\Classes\ControllerLogic\Employee\GetOneEmployeeLogic;
    use Illuminate\Http\Request;
    use Illuminate\View\View;

    class GetEmployeeEditController
    {
        public function __construct(
            private GetDepartmentInArrayLogic  $getDepartmentLogic,
            private GetDesignationInArrayLogic $getDesignationLogic,
            private GetOneEmployeeLogic        $getOneEmployeeLogic
        ) { }
        public function execute(string $id):View
        {
            $departmentCollection = $this ->getDepartmentLogic->execute();
            $designationCollection = $this ->getDesignationLogic->execute();
            $employee = $this ->getOneEmployeeLogic->execute((int)$id);

            return view('Employee\Edit')
                ->with('employee',$employee)
                ->with('departments',$departmentCollection)
                ->with('designations',$designationCollection)
                ;
        }



    }