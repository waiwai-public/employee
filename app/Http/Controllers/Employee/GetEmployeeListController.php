<?php
namespace App\Http\Controllers\Employee;



    use App\Classes\ControllerLogic\Department\GetDepartmentInArrayLogic;
    use App\Classes\ControllerLogic\Designation\GetDesignationInArrayLogic;
    use App\Classes\ControllerLogic\Employee\GetEmployeeLogic;
    use App\Http\Controllers\Controller;
    use Illuminate\View\View;

    class GetEmployeeListController extends Controller
    {
public function __construct(
    private GetDepartmentInArrayLogic                          $getDepartmentLogic,
                            private GetDesignationInArrayLogic $getDesignationLogic,
                            private GetEmployeeLogic           $getEmployeeLogic
) { }

        public function execute():view
        {
            $departmentCollection = $this ->getDepartmentLogic->execute();
            $designationCollection = $this ->getDesignationLogic->execute();
            $employeeCollection = $this ->getEmployeeLogic->execute();
            return view('Employee\List')
                ->with('departments',$departmentCollection)
                         ->with('designations',$designationCollection)
                         ->with('employees',$employeeCollection)
            ;
        }


    }