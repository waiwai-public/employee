<?php

    namespace App\Http\Controllers\Employee;

    use App\Classes\ControllerLogic\Employee\PutEmployeeLogic;
    use App\Classes\Exception\ResourceConflictException;
    use Illuminate\Http\RedirectResponse;
    use Illuminate\Http\Request;
    use Illuminate\Support\ItemNotFoundException;
    use Illuminate\Validation\ValidationException;

    class PutEmployeeController
    {
        public function __construct(private PutEmployeeLogic $putEmployeeLogic,) { }
        public function execute(string $id,Request $request):RedirectResponse
        {

            try{
                $status = $this->putEmployeeLogic->execute($request,(int)$id);
                return redirect("employee/edit/".$id."?status=204");

            }catch (ValidationException $e)
            {
                return redirect("employee/edit/".$id."?status=403");
            }catch (ResourceConflictException $e)
            {
                return redirect("employee/edit/".$id."?status=409");
            }
            catch (ItemNotFoundException $e)
            {
                return redirect("employee/edit/".$id."?status=404");
            }

    }


    }