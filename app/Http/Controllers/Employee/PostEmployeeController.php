<?php

    namespace App\Http\Controllers\Employee;

    use App\Classes\ControllerLogic\Employee\PostEmployeeLogic;
    use App\Classes\Exception\ResourceConflictException;
    use Illuminate\Http\RedirectResponse;
    use Illuminate\Http\Request;
    use Illuminate\Support\ItemNotFoundException;
    use Illuminate\Validation\ValidationException;
    use Illuminate\View\View;

    class PostEmployeeController
    {
public function __construct(private PostEmployeeLogic $postEmployeeLogic) { }
        public function execute(Request $request):RedirectResponse
        {
            try{
                $this->postEmployeeLogic->execute($request);
                return redirect("employee\add?status=204");

            }catch (ValidationException $e)
            {
                return redirect("employee\add?status=403");
            }catch (ResourceConflictException $e)
            {
                return redirect("employee\add?status=409");
            }
    }


    }