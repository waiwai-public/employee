<?php

    namespace App\Http\Controllers\Employee;

    use App\Classes\ControllerLogic\Department\GetDepartmentInArrayLogic;
    use App\Classes\ControllerLogic\Designation\GetDesignationInArrayLogic;
    use App\Classes\ControllerLogic\Employee\DeleteEmployeeLogic;
    use App\Classes\ControllerLogic\Employee\GetEmployeeLogic;
    use Illuminate\Http\RedirectResponse;
    use Illuminate\Support\ItemNotFoundException;
    use Illuminate\View\View;

    class DeleteEmployeeController
    {
        public
        function __construct(
            private DeleteEmployeeLogic        $deleteEmployeeLogic,
        )
        {
        }

        public
        function execute(string $id)
        : RedirectResponse
        {
            try {
                $this -> deleteEmployeeLogic -> execute((int)$id);
                return redirect("employee/list?status=204");

            }
            catch (ItemNotFoundException $e)
            {
                return redirect("employee/list?status=404");
            }



        }


    }