<?php

    namespace App\Http\Controllers\Department;
    use App\Classes\ControllerLogic\Department\PostDepartmentLogic;
    use App\Classes\Exception\ResourceConflictException;
    use App\Http\Controllers\Controller;
    use Illuminate\Http\Request;
    use Illuminate\Validation\ValidationException;

    class PostDepartmentController extends Controller
    {
public function __construct(private PostDepartmentLogic $postDepartmentLogic) { }
        public function execute(Request $request)
        {

            try{
                $this->postDepartmentLogic->execute($request);
                $status = 204;
            }catch (ResourceConflictException)
            {
                $status = 409;

            }catch (ValidationException)
            {
                $status = 403;
            }

            return redirect(route('department.add',['status'=>$status]));

        }


    }