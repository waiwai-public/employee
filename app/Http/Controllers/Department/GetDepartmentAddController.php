<?php

    namespace App\Http\Controllers\Department;

    use Illuminate\View\View;

    class GetDepartmentAddController
    {
public function __construct() { }
        public function execute():View
        {

            return view('Department/add');
        }


    }