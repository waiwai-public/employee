Code Flow
-
routes-> web.php  -- all web url route to access showing at there

code flow for written flow
1) app/Http/Controllers/Employee/   (Base Controller Connect With Laravel)
2) app/Classes/ControllerLogic      (Custom Controller Logic)
3) app/Classes/Validator            (DataValidator)
4) app/Classes/Exception            (Custom Exception)
5) app/Models                       (Data Models)
6) resources/views/Employee         (View File(Html file))
   

Deployment for local clone testing
-
Prerequisite - (Docker Desktop any version for window or Docker any version for linux) , Docker-compose v1 ( if Docker-compose version different will have different syntax , do let me know if you want to try running it on other docker-compose version)

1) Go into Project Directory
2) rename .env.example to .env
3) on docker-compose.yml , change all "C:\xampp/htdocs" to the project directory full path
4) Open Cmd
5) Type Docker-compose up ( do make sure port 80 and 3306 is not in used on host machine)
6) in window using docker desktop, go to containers find employee container,on php:8fpm press Open In Terminal Button
7) in Linux on powershell type docker container ls, find php:8-fpm container, type docker exec -it "the php:8-fpm container id" /bin/bash
8) Run This few Command Separately to install all required Component step 9 to step 14
9) apt update && apt install -y wget unzip curl
10) docker-php-ext-install mysqli pdo pdo_mysql
11) curl -sS https://getcomposer.org/installer |php
12) mv composer.phar /usr/local/bin/composer
13) composer install
14) php artisan key:generate
15) php artisan migrate
16) Restart the container employee, and it should be good to go
